package com.synechron.kafkaconsumer.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MessageConsumer {

    private static int counter = 0;

    @KafkaListener(groupId = "Loan-processor", topics = "loans-topic-new" ,id = "listener-1" )
    public void processLoan1(String loan){
        counter++;
        System.out.println("Came inside the processLoans method ");
        System.out.println("Loan message payload:: %%%%%% GROUP-ID: Pradeep %%%%%% "+loan);
        System.out.println(" Latest counter value = " + counter);
    }
}